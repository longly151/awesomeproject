import React, {Component} from 'react'
import {Text, View} from 'react-native'

export class AlignItemsExample extends Component {
  render() {
    return (
      <View style={{
        backgroundColor: 'aquamarine',
        // flex: 1,
        height: 500,
        flexDirection: 'row',

        justifyContent: 'center',
        alignItems: 'flex-end'
      }}>
        <Text style={{width: 50, height: 50, backgroundColor: 'red'}} />
        <Text style={{width: 50, height: 50, backgroundColor: 'green'}} />
        <Text style={{width: 50, height: 50, backgroundColor: 'blue'}} />
      </View>
    );
  }
}

export default AlignItemsExample
