import React, {Component} from 'react'
import {Text, View} from 'react-native'

export class JustifyContentExample extends Component {
  render() {
    return (
      <View style={{
        backgroundColor: 'aquamarine',
        // flex: 1,
        height: 500,
        flexDirection: 'row',
        // justifyContent: 'flex-start'
        // justifyContent: 'flex-end'
        // justifyContent: 'space-between',
        // justifyContent: 'space-around',
        // justifyContent: 'center',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'stretch'
        // alignItems: 'stretch'
      }}>
        <Text style={{width: 50, height: 50, backgroundColor: 'red'}} />
        <Text style={{width: 50, height: 50, backgroundColor: 'green'}} />
        <Text style={{width: 50, height: 50, backgroundColor: 'blue'}} />
      </View>
    );
  }
}

export default JustifyContentExample
