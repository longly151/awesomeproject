import React, {Component} from 'react'
import {Text, View} from 'react-native'

export class FlexExample extends Component {
  render() {
    return (
      <View style={{flex: 1, flexDirection: 'row', margin: 20}}>
        <Text style={{width: 50, height: 50, backgroundColor: 'red', marginBottom: 5}} />
        <Text style={{width: 50, height: 50, backgroundColor: 'green', marginBottom: 5}} />
        <Text style={{width: 50, height: 50, backgroundColor: 'blue'}} />
      </View>
    );
  }
}

export default FlexExample
