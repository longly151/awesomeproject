import React, {Component} from 'react'
import {StyleSheet, Text, View} from 'react-native'

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10,
    backgroundColor: '#28B463',
    borderWidth: 2,
    borderColor: '#1F618D',
  },
  firstText: {
    margin: 20, color: 'red'
  },
  secondText: {
    margin: 5,
    color: 'yellow',
    fontWeight: 'bold',
    fontSize: 20,
  }
})
export class Style extends Component {
  render() {
    return (
      <View>
        <Text style={styles.firstText}>First line</Text>
        <Text style={styles.secondText}>Second line</Text>
        <Text style={[styles.firstText, styles.secondText]}>first, second</Text>
        <Text style={[styles.secondText, styles.firstText]}>second, first</Text>
      </View>
    )
  }
}

export default Style
