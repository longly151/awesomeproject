/**
 * @format
 */

import {AppRegistry} from 'react-native';
// import App from './App';
import {name as appName} from './app.json';

import MultipleGreetings from './src/components/03-Basic/MultipleGreetings';
import Robot from './src/components/03-Basic/Robot';

import TextBlink from './src/components/05-Textblink/TextBlink';

import Style from './src/components/06-Style/Style';

import FixedDimension from './src/components/07-Flex/FixedDimension';
import FlexDimension from './src/components/07-Flex/FlexDimension';

import FlexExample from './src/components/08-JustifyContent-AlignItems/FlexExample';
import JustifyContentExample from './src/components/08-JustifyContent-AlignItems/JustifyContentExample';
import AlignItemsExample from './src/components/08-JustifyContent-AlignItems/AlignItemsExample';

import Form from './src/components/10-Form/Form';
import AdvanceForm from './src/components/10-Form/AdvanceForm';

import ButtonExample from './src/components/12-Button/ButtonExample';
import ButtonAdvance from './src/components/12-Button/ButtonAdvance';

import VerticalScrollView from './src/components/14-ScrollView/VerticalScrollView';
import HorizontalScrollView from './src/components/14-ScrollView/HorizontalScrollView';
import ViewPagerAndroidExample from './src/components/14-ScrollView/ViewPagerAndroidExample';

import FlatListExample from './src/components/17-FlatListExample/FlatListExample';
import HorizontalFlatList from './src/components/17-FlatListExample/HorizontalFlatList';
import SectionListExample from './src/components/17-FlatListExample/SectionListExample';

import LifeCycleComponent from './src/components/25-LifeCycle/LifeCycleComponent';

// import {createStackNavigator, createAppContainer} from 'react-navigation';
// // React Navigation
// import MainComponent from './src/components/46-ReactNavigation/MainComponent';
// import DetailComponent from './src/components/46-ReactNavigation/DetailComponent';
// import ThirdComponent from './src/components/46-ReactNavigation/ThirdComponent';
// // Screen names
// import {MainScreen, DetailScreen, ThirdScreen} from './screenNames';

// const AppNavigator = createStackNavigator({
//   MainScreen: {
//     screen: MainComponent,
//     navigationOptions: {
//       headerTitle: 'Main'
//     },
//   },
//   DetailScreen: {
//     screen: DetailComponent,
//     navigationOptions: {
//       headerTitle: 'Detail'
//     },
//   },
//   ThirdScreen: {
//     screen: ThirdComponent,
//     navigationOptions: {
//       headerTitle: 'Third'
//     },
//   },
// });

AppRegistry.registerComponent(appName, () => HorizontalFlatList);